package com.killer4android.guy.menglee.android.killer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by menglee on 2/24/2016.
 */
public class Deck {
    private final int CLUB = 1;
    private final int DIAMOND = 2;
    private final int HEART = 3;
    private final int SPADE = 4;
    private List<Card> deck = new ArrayList<>();

    public Deck() {
    }

    public List<Card> makeDeck() {
        makeDeckHelper(makeCardsHelper(CLUB));
        makeDeckHelper(makeCardsHelper(DIAMOND));
        makeDeckHelper(makeCardsHelper(HEART));
        makeDeckHelper(makeCardsHelper(SPADE));
        return deck;
    }

    private void makeDeckHelper(List<Card> cards) {
        for (Card card : cards) {
            deck.add(card);
        }
    }

    private List<Card> makeCardsHelper(int suit) {
        List<Card> cards = new ArrayList<>();
        for (int i = 3; i < 16; i++) {
            Card card = new Card(i, suit);
            cards.add(card);
        }
        return cards;
    }
}
