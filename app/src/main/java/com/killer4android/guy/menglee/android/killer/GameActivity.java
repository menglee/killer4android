package com.killer4android.guy.menglee.android.killer;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    //private static final String TAG = "GameActivity";
    private ImageView passButton;
    private ImageView tableOne;
    private ImageView tableTwo;
    private ImageView tableThree;
    private ImageView tableFour;
    private ImageView tableFive;
    private ImageView tableSix;

    private ImageView[] tableArr = { tableOne, tableTwo, tableThree, tableFour, tableFive, tableSix };

    private ImageView cardOne;
    private ImageView cardTwo;
    private ImageView cardThree;
    private ImageView cardFour;
    private ImageView cardFive;
    private ImageView cardSix;
    private ImageView cardSeven;
    private ImageView cardEight;
    private ImageView cardNine;
    private ImageView cardTen;
    private ImageView cardEleven;
    private ImageView cardTwelve;
    private ImageView cardThirteen;

    private ImageView[] handArr = { cardOne, cardTwo, cardThree, cardFour, cardFive, cardSix, cardSeven,
            cardEight, cardNine, cardTen, cardEleven, cardTwelve, cardThirteen };

    private TextView p1InfoText;
    private TextView p1PassText;
    private TextView p2PassText;
    private TextView p3PassText;
    private TextView p4PassText;
    private TextView playerWins;

    private HashMap<Integer, String> cardMap = new HashMap<>();
    private List<Boolean> selected = new ArrayList<>();
    private List<Boolean> passes = new ArrayList<>();
    private HashMap<Card, ImageView> playMap = new HashMap<>();

    private Deck d;
    private Dealer deal;
    private Table table;
    private Player p1;
    private Ai p2;
    private Ai p3;
    private Ai p4;

    private ProgressBar p2Progress;
    private ProgressBar p3Progress;
    private ProgressBar p4Progress;

    private boolean isClickable;
    private boolean p1WinFlag;
    private boolean p2WinFlag;
    private boolean p3WinFlag;
    private boolean p4WinFlag;

    private Button playAgain;

    private MediaPlayer mpBoing;
    private MediaPlayer mpClick;
    private MediaPlayer mpGo;
    private MediaPlayer mpNoo;
    private MediaPlayer mpShuffle;
    private MediaPlayer mpSwoosh;
    private MediaPlayer mpTada;

    private boolean isActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        getReferencesToViews();
        makeCardMap();

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    private void getReferencesToViews() {
        passButton = (ImageView) findViewById(R.id.pass_button);
        passButton.setOnClickListener(this);

        tableOne = (ImageView) findViewById(R.id.table_one);
        tableTwo = (ImageView) findViewById(R.id.table_two);
        tableThree = (ImageView) findViewById(R.id.table_three);
        tableFour = (ImageView) findViewById(R.id.table_four);
        tableFive = (ImageView) findViewById(R.id.table_five);
        tableSix = (ImageView) findViewById(R.id.table_six);

        tableArr[0] = tableOne;
        tableArr[1] = tableTwo;
        tableArr[2] = tableThree;
        tableArr[3] = tableFour;
        tableArr[4] = tableFive;
        tableArr[5] = tableSix;

        cardOne = (ImageView) findViewById(R.id.card_one);
        cardTwo = (ImageView) findViewById(R.id.card_two);
        cardThree = (ImageView) findViewById(R.id.card_three);
        cardFour = (ImageView) findViewById(R.id.card_four);
        cardFive = (ImageView) findViewById(R.id.card_five);
        cardSix = (ImageView) findViewById(R.id.card_six);
        cardSeven = (ImageView) findViewById(R.id.card_seven);
        cardEight = (ImageView) findViewById(R.id.card_eight);
        cardNine = (ImageView) findViewById(R.id.card_nine);
        cardTen = (ImageView) findViewById(R.id.card_ten);
        cardEleven = (ImageView) findViewById(R.id.card_eleven);
        cardTwelve = (ImageView) findViewById(R.id.card_twelve);
        cardThirteen = (ImageView) findViewById(R.id.card_thirteen);

        handArr[0] = cardOne;
        handArr[1] = cardTwo;
        handArr[2] = cardThree;
        handArr[3] = cardFour;
        handArr[4] = cardFive;
        handArr[5] = cardSix;
        handArr[6] = cardSeven;
        handArr[7] = cardEight;
        handArr[8] = cardNine;
        handArr[9] = cardTen;
        handArr[10] = cardEleven;
        handArr[11] = cardTwelve;
        handArr[12] = cardThirteen;

        cardOne.setOnClickListener(this);
        cardTwo.setOnClickListener(this);
        cardThree.setOnClickListener(this);
        cardFour.setOnClickListener(this);
        cardFive.setOnClickListener(this);
        cardSix.setOnClickListener(this);
        cardSeven.setOnClickListener(this);
        cardEight.setOnClickListener(this);
        cardNine.setOnClickListener(this);
        cardTen.setOnClickListener(this);
        cardEleven.setOnClickListener(this);
        cardTwelve.setOnClickListener(this);
        cardThirteen.setOnClickListener(this);

        for(int i = 0; i < 13; i++) {
            selected.add(i, false);
        }

        p2Progress = (ProgressBar) findViewById(R.id.player_two_progress);
        p3Progress = (ProgressBar) findViewById(R.id.player_three_progress);
        p4Progress = (ProgressBar) findViewById(R.id.player_four_progress);

        p1InfoText = (TextView) findViewById(R.id.player_one_text);
        p1PassText = (TextView) findViewById(R.id.player_one_pass_text);
        p2PassText = (TextView) findViewById(R.id.player_two_pass_text);
        p3PassText = (TextView) findViewById(R.id.player_three_pass_text);
        p4PassText = (TextView) findViewById(R.id.player_four_pass_text);
        playerWins = (TextView) findViewById(R.id.player_wins);

        p1InfoText.setVisibility(View.VISIBLE);
        p1PassText.setVisibility(View.INVISIBLE);
        p2PassText.setVisibility(View.INVISIBLE);
        p3PassText.setVisibility(View.INVISIBLE);
        p4PassText.setVisibility(View.INVISIBLE);
        playerWins.setVisibility(View.INVISIBLE);

        playAgain = (Button) findViewById(R.id.play_again);
        playAgain.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {
                if (isActive)
                    mpShuffle.start();

                d = new Deck();
                deal = new Dealer(d.makeDeck());
                deal.shuffle();
                deal.deal();
                table = new Table();

                isClickable = true;
                p1WinFlag = false;
                p2WinFlag = false;
                p3WinFlag = false;
                p4WinFlag = false;

                passes.clear();

                p1 = new Player(deal.getHandOne(), table);
                for (int i = 0; i < 13; i++) {
                    handArr[i].setVisibility(View.VISIBLE);
                }
                updateHand(p1.HAND());
                updateTable();
                clearTableView();

                p2 = new Ai(deal.getHandTwo(), table);
                p3 = new Ai(deal.getHandThree(), table);
                p4 = new Ai(deal.getHandFour(), table);

                playAgain.setVisibility(View.INVISIBLE);
                playerWins.setVisibility(View.INVISIBLE);
                p2PassText.setText(R.string.pass_note);
                p2PassText.setVisibility(View.INVISIBLE);
                p3PassText.setText(R.string.pass_note);
                p2PassText.setVisibility(View.INVISIBLE);
                p4PassText.setText(R.string.pass_note);
                p4PassText.setVisibility(View.INVISIBLE);
            }

        });
        playAgain.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        isActive = true;
        isClickable = true;

        mpBoing = MediaPlayer.create(getApplicationContext(), R.raw.boing);
        mpClick = MediaPlayer.create(getApplicationContext(), R.raw.click);
        mpGo = MediaPlayer.create(getApplicationContext(), R.raw.go);
        mpGo.setVolume(0.7f, 0.7f);
        mpNoo = MediaPlayer.create(getApplicationContext(), R.raw.noo);
        mpShuffle = MediaPlayer.create(getApplicationContext(), R.raw.shuffle);
        mpSwoosh = MediaPlayer.create(getApplicationContext(), R.raw.swoosh);
        mpTada = MediaPlayer.create(getApplicationContext(), R.raw.tada);

        if (isActive)
            mpShuffle.start();

        d = new Deck();
        deal = new Dealer(d.makeDeck());
        deal.shuffle();
        deal.deal();
        table = new Table();

        p1WinFlag = false;
        p2WinFlag = false;
        p3WinFlag = false;
        p4WinFlag = false;

        p1 = new Player(deal.getHandOne(), table);
        for (int i = 0; i < 13; i++) {
            handArr[i].setVisibility(View.VISIBLE);
        }
        updateHand(p1.HAND());
        updateTable();
        clearTableView();

        p2 = new Ai(deal.getHandTwo(), table);
        p3 = new Ai(deal.getHandThree(), table);
        p4 = new Ai(deal.getHandFour(), table);
    }

    @Override
    public void onPause() {
        super.onPause();
        isActive = false;
        isClickable = false;
        mpBoing.release();
        mpClick.release();
        mpGo.release();
        mpNoo.release();
        mpSwoosh.release();
        mpShuffle.release();
        mpTada.release();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (isActive) {
            int action = MotionEventCompat.getActionMasked(event);
            switch(action) {
                case (MotionEvent.ACTION_DOWN):
                    return true;
                case (MotionEvent.ACTION_MOVE):
                    return true;
                case (MotionEvent.ACTION_UP):
                    checkQ(passes, table);
                    if (playMap.size() > 0 && p1.evaluate(playMap)) {
                        mpSwoosh.start();
                        p1InfoText.setVisibility(View.INVISIBLE);
                        passes.add(true);
                        updateTable();
                        updateHand(p1.HAND());
                        p4PassText.setVisibility(View.INVISIBLE);
                        if (p1.CARDS() == 0) {
                            mpTada.start();
                            p1WinFlag = true;
                            isClickable = true;
                            resetCards(playMap);
                            playerWins.setVisibility(View.VISIBLE);

                            resetGame();
                        } else {
                            resetCards(playMap);
                            isClickable = false;
                            runAi();
                        }
                    } else {
                        mpBoing.start();
                        if (playMap.size() > 0) {
                            resetCards(playMap);
                        }
                    }
                    return true;
                case (MotionEvent.ACTION_CANCEL):
                    return true;
                case (MotionEvent.ACTION_OUTSIDE):
                    return true;
                default:
                    return super.onTouchEvent(event);
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onClick(View v) {
        p1PassText.setVisibility(View.INVISIBLE);
        if (isClickable) {
            switch (v.getId()) {
                case R.id.pass_button: {
                    if (isActive)
                        mpGo.start();
                    p1PassText.setVisibility(View.VISIBLE);
                    p4PassText.setVisibility(View.INVISIBLE);
                    p1InfoText.setVisibility(View.INVISIBLE);
                    passes.add(false);
                    resetCards(playMap);
                    isClickable = false;

                    if (isActive)
                        runAi();
                    break;
                }
                case R.id.card_one: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(0)) {
                        mParams.bottomMargin += 32;
                        selected.set(0, true);
                        playMap.put(p1.HAND().get(0), cardOne);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(0, false);
                        playMap.remove(p1.HAND().get(0));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_two: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(1)) {
                        mParams.bottomMargin += 32;
                        selected.set(1, true);
                        playMap.put(p1.HAND().get(1), cardTwo);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(1, false);
                        playMap.remove(p1.HAND().get(1));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_three: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(2)) {
                        mParams.bottomMargin += 32;
                        selected.set(2, true);
                        playMap.put(p1.HAND().get(2), cardThree);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(2, false);
                        playMap.remove(p1.HAND().get(2));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_four: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(3)) {
                        mParams.bottomMargin += 32;
                        selected.set(3, true);
                        playMap.put(p1.HAND().get(3), cardFour);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(3, false);
                        playMap.remove(p1.HAND().get(3));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_five: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(4)) {
                        mParams.bottomMargin += 32;
                        selected.set(4, true);
                        playMap.put(p1.HAND().get(4), cardFive);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(4, false);
                        playMap.remove(p1.HAND().get(4));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_six: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(5)) {
                        mParams.bottomMargin += 32;
                        selected.set(5, true);
                        playMap.put(p1.HAND().get(5), cardSix);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(5, false);
                        playMap.remove(p1.HAND().get(5));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_seven: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(6)) {
                        mParams.bottomMargin += 32;
                        selected.set(6, true);
                        playMap.put(p1.HAND().get(6), cardSeven);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(6, false);
                        playMap.remove(p1.HAND().get(6));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_eight: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(7)) {
                        mParams.bottomMargin += 32;
                        selected.set(7, true);
                        playMap.put(p1.HAND().get(7), cardEight);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(7, false);
                        playMap.remove(p1.HAND().get(7));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_nine: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(8)) {
                        mParams.bottomMargin += 32;
                        selected.set(8, true);
                        playMap.put(p1.HAND().get(8), cardNine);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(8, false);
                        playMap.remove(p1.HAND().get(8));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_ten: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(9)) {
                        mParams.bottomMargin += 32;
                        selected.set(9, true);
                        playMap.put(p1.HAND().get(9), cardTen);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(9, false);
                        playMap.remove(p1.HAND().get(9));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_eleven: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(10)) {
                        mParams.bottomMargin += 32;
                        selected.set(10, true);
                        playMap.put(p1.HAND().get(10), cardEleven);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(10, false);
                        playMap.remove(p1.HAND().get(10));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_twelve: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(11)) {
                        mParams.bottomMargin += 32;
                        selected.set(11, true);
                        playMap.put(p1.HAND().get(11), cardTwelve);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(11, false);
                        playMap.remove(p1.HAND().get(11));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
                case R.id.card_thirteen: {
                    RelativeLayout.MarginLayoutParams mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
                    if (isActive)
                        mpClick.start();
                    if (!selected.get(12)) {
                        mParams.bottomMargin += 32;
                        selected.set(12, true);
                        playMap.put(p1.HAND().get(12), cardThirteen);
                    }
                    else {
                        mParams.bottomMargin -= 32;
                        selected.set(12, false);
                        playMap.remove(p1.HAND().get(12));
                    }
                    v.setLayoutParams(mParams);
                    break;
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        p1InfoText.setVisibility(View.VISIBLE);
    }

    private void runAi() {
        p2Progress.setProgress(100);
        if (isActive) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(2200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            p2Progress.setProgress(0);
                            //------------------------------------------------------------- RUN AI 2 HERE ------------
                            if (p1WinFlag) {

                            } else {
                                p1PassText.setVisibility(View.INVISIBLE);
                                checkQ(passes, table);
                                passes.add(p2.play());
                                updateTable();
                                if (!passes.get(passes.size() - 1)) {
                                    if (isActive)
                                        mpGo.start();
                                    p2PassText.setVisibility(View.VISIBLE);
                                } else {
                                    if (isActive)
                                        mpSwoosh.start();
                                }

                                if (p2.CARDS() == 0) {
                                    isClickable = false;
                                    p2WinFlag = true;
                                    if (isActive)
                                        mpNoo.start();
                                    p2PassText.setText(R.string.i_win);
                                    p2PassText.setVisibility(View.VISIBLE);
                                    isClickable = false;
                                    resetGame();
                                } else {
                                    p3Progress.setProgress(100);
                                }
                            }
                            //----------------------------------------------------------------------------------------
                            if (isActive) {
                                Thread thread2 = new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            Thread.sleep(1900);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                p3Progress.setProgress(0);
                                                //--------------------------------------------- RUN AI 3 HERE -------------
                                                if (p2WinFlag || p1WinFlag) {

                                                } else {
                                                    p2PassText.setVisibility(View.INVISIBLE);
                                                    checkQ(passes, table);
                                                    passes.add(p3.play());
                                                    updateTable();

                                                    if (!passes.get(passes.size() - 1)) {
                                                        if (isActive)
                                                            mpGo.start();
                                                        p3PassText.setVisibility(View.VISIBLE);
                                                    } else {
                                                        if (isActive)
                                                            mpSwoosh.start();
                                                    }
                                                    if (p3.CARDS() == 0) {
                                                        isClickable = false;
                                                        p3WinFlag = true;
                                                        if (isActive)
                                                            mpNoo.start();
                                                        p3PassText.setText(R.string.i_win);
                                                        p3PassText.setVisibility(View.VISIBLE);
                                                        isClickable = false;
                                                        resetGame();
                                                    } else {
                                                        p4Progress.setProgress(100);
                                                    }
                                                }
                                                //-------------------------------------------------------------------------
                                                if (isActive) {
                                                    Thread thread3 = new Thread() {

                                                        @Override

                                                        public void run() {
                                                            try {
                                                                Thread.sleep(2000);
                                                            } catch (InterruptedException e) {
                                                                e.printStackTrace();
                                                            }
                                                            runOnUiThread(new Runnable() {

                                                                @Override

                                                                public void run() {
                                                                    p4Progress.setProgress(0);
                                                                    //----------------------------- RUN AI 4 HERE -------------
                                                                    if (p3WinFlag || p2WinFlag || p1WinFlag) {

                                                                    } else {
                                                                        p3PassText.setVisibility(View.INVISIBLE);
                                                                        checkQ(passes, table);
                                                                        passes.add(p4.play());

                                                                        if (!passes.get(passes.size() - 1)) {
                                                                            if (isActive)
                                                                                mpGo.start();
                                                                            p4PassText.setVisibility(View.VISIBLE);
                                                                        } else {
                                                                            if (isActive)
                                                                                mpSwoosh.start();
                                                                        }
                                                                        updateTable();

                                                                        if (p4.CARDS() == 0) {
                                                                            isClickable = false;
                                                                            p4WinFlag = true;
                                                                            if (isActive)
                                                                                mpNoo.start();
                                                                            p4PassText.setText(R.string.i_win);
                                                                            p4PassText.setVisibility(View.VISIBLE);
                                                                            isClickable = false;
                                                                            resetGame();
                                                                        } else {
                                                                            Thread thread4 = new Thread() {

                                                                                @Override

                                                                                public void run() {
                                                                                    try {
                                                                                        Thread.sleep(1000);
                                                                                    } catch (InterruptedException e) {
                                                                                        e.printStackTrace();
                                                                                    }
                                                                                    runOnUiThread(new Runnable() {

                                                                                        @Override

                                                                                        public void run() {
                                                                                            checkQ(passes, table);
                                                                                            p1InfoText.setVisibility(View.VISIBLE);
                                                                                        }

                                                                                    });
                                                                                }

                                                                            };
                                                                            thread4.start();
                                                                        }
                                                                    }
                                                                    isClickable = true;
                                                                    //---------------------------------------------------------
                                                                }

                                                            });
                                                        }

                                                    };
                                                    thread3.start();
                                                }
                                            }
                                        });
                                    }
                                };
                                thread2.start();
                            }
                        }
                    });
                }
            };
            thread.start();
        }
    }

    private void updateHand(List<Card> hand) {
        for(int i = 0; i < hand.size(); i++) {
            String suit = hand.get(i).toString();
            String str = cardMap.get(hand.get(i).toInt());
            switch (suit) {
                case "Club": {
                    updateClubView(handArr, str, i);
                    break;
                }
                case "Diamond": {
                    updateDiamondView(handArr, str, i);
                    break;
                }
                case "Heart": {
                    updateHeartView(handArr, str, i);
                    break;
                }
                case "Spade": {
                    updateSpadeView(handArr, str, i);
                    break;
                }
            }
        }
        for (int i = hand.size(); i < 13; i++) {
            handArr[i].setVisibility(View.GONE);
        }
    }

    private void updateTable() {
        for(int i = 0; i < table.getState().size(); i++) {
            String suit = table.getState().get(i).toString();
            String str = cardMap.get(table.getState().get(i).toInt());
            switch (suit) {
                case "Club": {
                    updateClubView(tableArr, str, i);
                    break;
                }
                case "Diamond": {
                    updateDiamondView(tableArr, str, i);
                    break;
                }
                case "Heart": {
                    updateHeartView(tableArr, str, i);
                    break;
                }
                case "Spade": {
                    updateSpadeView(tableArr, str, i);
                    break;
                }
            }
        }
    }

    private void updateClubView(ImageView[] arr, String str, int idx) {
        switch (str) {
            case "c01": {
                arr[idx].setImageResource(R.drawable.c01);
                break;
            }
            case "c02": {
                arr[idx].setImageResource(R.drawable.c02);
                break;
            }
            case "c03": {
                arr[idx].setImageResource(R.drawable.c03);
                break;
            }
            case "c04": {
                arr[idx].setImageResource(R.drawable.c04);
                break;
            }
            case "c05": {
                arr[idx].setImageResource(R.drawable.c05);
                break;
            }
            case "c06": {
                arr[idx].setImageResource(R.drawable.c06);
                break;
            }
            case "c07": {
                arr[idx].setImageResource(R.drawable.c07);
                break;
            }
            case "c08": {
                arr[idx].setImageResource(R.drawable.c08);
                break;
            }
            case "c09": {
                arr[idx].setImageResource(R.drawable.c09);
                break;
            }
            case "c10": {
                arr[idx].setImageResource(R.drawable.c10);
                break;
            }
            case "c11": {
                arr[idx].setImageResource(R.drawable.c11);
                break;
            }
            case "c12": {
                arr[idx].setImageResource(R.drawable.c12);
                break;
            }
            case "c13": {
                arr[idx].setImageResource(R.drawable.c13);
                break;
            }
        }
    }

    private void updateDiamondView(ImageView[] arr, String str, int idx) {
        switch (str) {
            case "d01": {
                arr[idx].setImageResource(R.drawable.d01);
                break;
            }
            case "d02": {
                arr[idx].setImageResource(R.drawable.d02);
                break;
            }
            case "d03": {
                arr[idx].setImageResource(R.drawable.d03);
                break;
            }
            case "d04": {
                arr[idx].setImageResource(R.drawable.d04);
                break;
            }
            case "d05": {
                arr[idx].setImageResource(R.drawable.d05);
                break;
            }
            case "d06": {
                arr[idx].setImageResource(R.drawable.d06);
                break;
            }
            case "d07": {
                arr[idx].setImageResource(R.drawable.d07);
                break;
            }
            case "d08": {
                arr[idx].setImageResource(R.drawable.d08);
                break;
            }
            case "d09": {
                arr[idx].setImageResource(R.drawable.d09);
                break;
            }
            case "d10": {
                arr[idx].setImageResource(R.drawable.d10);
                break;
            }
            case "d11": {
                arr[idx].setImageResource(R.drawable.d11);
                break;
            }
            case "d12": {
                arr[idx].setImageResource(R.drawable.d12);
                break;
            }
            case "d13": {
                arr[idx].setImageResource(R.drawable.d13);
                break;
            }
        }
    }

    private void updateHeartView(ImageView[] arr, String str, int idx) {
        switch (str) {
            case "h01": {
                arr[idx].setImageResource(R.drawable.h01);
                break;
            }
            case "h02": {
                arr[idx].setImageResource(R.drawable.h02);
                break;
            }
            case "h03": {
                arr[idx].setImageResource(R.drawable.h03);
                break;
            }
            case "h04": {
                arr[idx].setImageResource(R.drawable.h04);
                break;
            }
            case "h05": {
                arr[idx].setImageResource(R.drawable.h05);
                break;
            }
            case "h06": {
                arr[idx].setImageResource(R.drawable.h06);
                break;
            }
            case "h07": {
                arr[idx].setImageResource(R.drawable.h07);
                break;
            }
            case "h08": {
                arr[idx].setImageResource(R.drawable.h08);
                break;
            }
            case "h09": {
                arr[idx].setImageResource(R.drawable.h09);
                break;
            }
            case "h10": {
                arr[idx].setImageResource(R.drawable.h10);
                break;
            }
            case "h11": {
                arr[idx].setImageResource(R.drawable.h11);
                break;
            }
            case "h12": {
                arr[idx].setImageResource(R.drawable.h12);
                break;
            }
            case "h13": {
                arr[idx].setImageResource(R.drawable.h13);
                break;
            }
        }
    }

    private void updateSpadeView(ImageView[] arr, String str, int idx) {
        switch (str) {
            case "s01": {
                arr[idx].setImageResource(R.drawable.s01);
                break;
            }
            case "s02": {
                arr[idx].setImageResource(R.drawable.s02);
                break;
            }
            case "s03": {
                arr[idx].setImageResource(R.drawable.s03);
                break;
            }
            case "s04": {
                arr[idx].setImageResource(R.drawable.s04);
                break;
            }
            case "s05": {
                arr[idx].setImageResource(R.drawable.s05);
                break;
            }
            case "s06": {
                arr[idx].setImageResource(R.drawable.s06);
                break;
            }
            case "s07": {
                arr[idx].setImageResource(R.drawable.s07);
                break;
            }
            case "s08": {
                arr[idx].setImageResource(R.drawable.s08);
                break;
            }
            case "s09": {
                arr[idx].setImageResource(R.drawable.s09);
                break;
            }
            case "s10": {
                arr[idx].setImageResource(R.drawable.s10);
                break;
            }
            case "s11": {
                arr[idx].setImageResource(R.drawable.s11);
                break;
            }
            case "s12": {
                arr[idx].setImageResource(R.drawable.s12);
                break;
            }
            case "s13": {
                arr[idx].setImageResource(R.drawable.s13);
                break;
            }
        }
    }

    private void checkQ(List<Boolean> passes, Table table) {
        if (passes.size() < 3)
            return;
        int counter = passes.size() - 1;
        if (!passes.get(counter) && !passes.get(counter - 1) && !passes.get(counter - 2)) {
            table.clear();
            passes.clear();
            clearTableView();
        }
    }

    private void clearTableView() {
        for(int i = 0; i < 6; i++) {
            tableArr[i].setImageResource(0);
        }
        p1PassText.setVisibility(View.INVISIBLE);
        p2PassText.setVisibility(View.INVISIBLE);
        p3PassText.setVisibility(View.INVISIBLE);
        p4PassText.setVisibility(View.INVISIBLE);
    }

    private void resetGame() {
        playAgain.setVisibility(View.VISIBLE);
        p1InfoText.setVisibility(View.INVISIBLE);
    }

    private void resetCards(HashMap<Card, ImageView> reset) {
        RelativeLayout.MarginLayoutParams mParams;
        for(Map.Entry member: reset.entrySet()) {
            ImageView v = (ImageView) member.getValue();
            mParams = (RelativeLayout.MarginLayoutParams) v.getLayoutParams();
            mParams.bottomMargin -= 32;
            v.setLayoutParams(mParams);
        }
        playMap.clear();
        for(int i = 0; i < selected.size(); i++) {
            selected.set(i, false);
        }
    }

    private void makeCardMap() {
        cardMap.put(31, "c03");
        cardMap.put(41, "c04");
        cardMap.put(51, "c05");
        cardMap.put(61, "c06");
        cardMap.put(71, "c07");
        cardMap.put(81, "c08");
        cardMap.put(91, "c09");
        cardMap.put(101, "c10");
        cardMap.put(111, "c11");
        cardMap.put(121, "c12");
        cardMap.put(131, "c13");
        cardMap.put(141, "c01");
        cardMap.put(151, "c02");

        cardMap.put(32, "d03");
        cardMap.put(42, "d04");
        cardMap.put(52, "d05");
        cardMap.put(62, "d06");
        cardMap.put(72, "d07");
        cardMap.put(82, "d08");
        cardMap.put(92, "d09");
        cardMap.put(102, "d10");
        cardMap.put(112, "d11");
        cardMap.put(122, "d12");
        cardMap.put(132, "d13");
        cardMap.put(142, "d01");
        cardMap.put(152, "d02");

        cardMap.put(33, "h03");
        cardMap.put(43, "h04");
        cardMap.put(53, "h05");
        cardMap.put(63, "h06");
        cardMap.put(73, "h07");
        cardMap.put(83, "h08");
        cardMap.put(93, "h09");
        cardMap.put(103, "h10");
        cardMap.put(113, "h11");
        cardMap.put(123, "h12");
        cardMap.put(133, "h13");
        cardMap.put(143, "h01");
        cardMap.put(153, "h02");

        cardMap.put(34, "s03");
        cardMap.put(44, "s04");
        cardMap.put(54, "s05");
        cardMap.put(64, "s06");
        cardMap.put(74, "s07");
        cardMap.put(84, "s08");
        cardMap.put(94, "s09");
        cardMap.put(104, "s10");
        cardMap.put(114, "s11");
        cardMap.put(124, "s12");
        cardMap.put(134, "s13");
        cardMap.put(144, "s01");
        cardMap.put(154, "s02");
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mpBoing.release();
        mpClick.release();
        mpGo.release();
        mpNoo.release();
        mpSwoosh.release();
        mpShuffle.release();
        mpTada.release();
    }
}