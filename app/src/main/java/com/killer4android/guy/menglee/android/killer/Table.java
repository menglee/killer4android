package com.killer4android.guy.menglee.android.killer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by menglee on 2/26/2016.
 */
public class Table {

    private List<Card> state;

    public Table() {
        state = new ArrayList<>();
    }

    public List<Card> getState() {
        return state;
    }

    public void clear() {
        state = new ArrayList<>();
    }
}
