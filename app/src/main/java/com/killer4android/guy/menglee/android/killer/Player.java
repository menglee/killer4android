package com.killer4android.guy.menglee.android.killer;

import android.widget.ImageView;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by menglee on 2/29/2016.
 */
public class Player {
    //private static final String TAG = "GameActivity";
    private List<Card> hand = new ArrayList<>();
    private List<Card> orderedCards = new ArrayList<>();
    private Table table;
    private boolean first;

    public Player(List<Card> hand, Table table) {
        this.hand = hand;
        this.table = table;
        this.first = true;
    }

    public int CARDS() {
        return hand.size();
    }

    public List<Card> HAND() {
        return hand;
    }

    public boolean evaluate(HashMap<Card, ImageView> play) {
        orderedCards.clear();
        for(Map.Entry member: play.entrySet()) {
            Card card = (Card) member.getKey();
            orderedCards.add(card);
        }
        Collections.sort(orderedCards);

        if (table.getState().size() == 0) {
            if (orderedCards.size() == 1) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 2 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 3 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 3 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 5 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 1 && table.getState().get(0).getValue() < 10) {
            if (orderedCards.size() == 1 && orderedCards.get(0).getValue() >= table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 1 && table.getState().get(0).getValue() >= 10) {
            if (orderedCards.size() == 1 && orderedCards.get(0).getValue() == table.getState().get(0).getValue() && orderedCards.get(0).getSuit() > table.getState().get(0).getSuit()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 1 && orderedCards.get(0).getValue() > table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 2 && table.getState().get(0).getValue() < 10) {
            if (orderedCards.size() == 2 && isValidMatch(orderedCards) && orderedCards.get(0).getValue() >= table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 2 && table.getState().get(0).getValue() >= 10) {
            if (orderedCards.size() == 2 && orderedCards.get(0).getValue() == table.getState().get(0).getValue() && isValidMatch(orderedCards) && (orderedCards.get(0).getSuit() == 4 || orderedCards.get(1).getSuit() == 4)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 2 && isValidMatch(orderedCards) && orderedCards.get(0).getValue() > table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 3 && isValidMatch(table.getState())) {
            if (orderedCards.size() == 3 && isValidMatch(orderedCards) && orderedCards.get(0).getValue() > table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 3 && isValidStraight(table.getState())) {
            if (orderedCards.size() == 3 && isValidStraight(orderedCards) && orderedCards.get(0).getValue() >= table.getState().get(0).getValue() && orderedCards.get(0).getSuit() > table.getState().get(0).getSuit()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 4 && isValidMatch(table.getState())) {
            if (orderedCards.size() == 4 && isValidMatch(orderedCards) && orderedCards.get(0).getValue() > table.getState().get(0).getValue()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 4 && isValidStraight(table.getState())) {
            if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 4 && isValidStraight(orderedCards) && orderedCards.get(0).getValue() >= table.getState().get(0).getValue() && orderedCards.get(0).getSuit() > table.getState().get(0).getSuit()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 5 && isValidStraight(table.getState())) {
            if (orderedCards.size() == 4 && isValidMatch(orderedCards)) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 5 && isValidStraight(orderedCards) && orderedCards.get(0).getValue() >= table.getState().get(0).getValue() && orderedCards.get(0).getSuit() > table.getState().get(0).getSuit()) {
                update(orderedCards);
                return true;
            } else if (orderedCards.size() == 6 && isValidStraight(orderedCards)) {
                update(orderedCards);
                return true;
            }
            return false;
        } else if (table.getState().size() == 6 && isValidStraight(table.getState())) {
            if (orderedCards.size() == 6 && isValidStraight(orderedCards) && orderedCards.get(0).getValue() >= table.getState().get(0).getValue() && orderedCards.get(0).getSuit() > table.getState().get(0).getSuit()) {
                update(orderedCards);
                return true;
            }
            return false;
        }
        return false;
    }

    private void update(List<Card> cards) {
        table.getState().clear();
        for(Card card: cards) {
            hand.remove(card);
            table.getState().add(card);
        }
    }

    private boolean isValidMatch(List<Card> cards) {
        HashSet<Integer> set = new HashSet<>();
        for(Card card: cards)
            set.add(card.getValue());
        return set.size() == 1;
    }

    private boolean isValidStraight(List<Card> cards) {
        if (isValidFlush(cards)) {
            Deque<Integer> stack = new ArrayDeque<>();
            for(Card card: cards) {
                if (stack.size() == 0) {
                    stack.add(card.getValue());
                } else {
                    if (stack.getLast() == (card.getValue() - 1)) {
                        stack.add(card.getValue());
                    }
                }
            }
            if (stack.size() == cards.size())
                return true;
        }
        return false;
    }

    private boolean isValidFlush(List<Card> cards) {
        HashSet<Integer> set = new HashSet<>();
        for(Card card: cards)
            set.add(card.getSuit());
        return set.size() == 1;
    }
}
