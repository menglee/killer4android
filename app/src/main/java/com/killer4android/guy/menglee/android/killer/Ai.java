package com.killer4android.guy.menglee.android.killer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by menglee on 2/26/2016.
 */
public class Ai {
    //private static final String TAG = "GameActivity";
    private final int SINGLE = 1;
    private final int DOUBLE = 2;
    private final int TRIPLE = 3;
    private final int BOMB = 4;

    private List<Card> hand;
    private Table table;

    private List<Card> three = new ArrayList<>();
    private List<Card> four = new ArrayList<>();
    private List<Card> five = new ArrayList<>();
    private List<Card> six = new ArrayList<>();
    private List<Card> seven = new ArrayList<>();
    private List<Card> eight = new ArrayList<>();
    private List<Card> nine = new ArrayList<>();
    private List<Card> ten = new ArrayList<>();
    private List<Card> eleven = new ArrayList<>();
    private List<Card> twelve = new ArrayList<>();
    private List<Card> thirteen = new ArrayList<>();
    private List<Card> fourteen = new ArrayList<>();
    private List<Card> fifteen = new ArrayList<>();

    private List<List<Card>> preSingles = new ArrayList<>();
    private List<List<Card>> preDoubles = new ArrayList<>();
    private List<List<Card>> preTriples = new ArrayList<>();
    private List<List<Card>> preBomb = new ArrayList<>();

    public Ai(List<Card> hand, Table table) {
        this.hand = hand;
        this.table = table;
    }

    public int CARDS() {
        return hand.size();
    }

    public boolean play() {
        organizeCards();
        List<Card> singles = getSingles();
        List<Card> doubles = getDoubles();
        List<Card> triples = getTriples();
        List<Card> bombs = getBombs();
        clear();

        if (table.getState().size() == 0) {
            if (triples.size() > 0 && triples.get(0).getValue() < 10) {
                update(triples, TRIPLE);
                return true;
            } else if (singles.size() > 0) {
                update(singles, SINGLE);
                return true;
            } else if (doubles.size() > 0) {
                update(doubles, DOUBLE);
                return true;
            } else if (triples.size() > 0) {
                update(triples, TRIPLE);
                return true;
            } else if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 1 && table.getState().get(0).getValue() < 10) {
            singles = getGreaterThanOrEqual(singles);
            if (singles.size() > 0) {
                update(singles, SINGLE);
                return true;
            } else if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 1 && table.getState().get(0).getValue() >= 10) {
            singles = getGreaterThanOrEqual(singles);
            if (singles.size() > 0 && singles.get(0).getValue() == table.getState().get(0).getValue() && singles.get(0).getSuit() < table.getState().get(0).getSuit()) {
                singles.remove(0);
            }
            if (singles.size() > 0) {
                update(singles, SINGLE);
                return true;
            } else if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 2 && table.getState().get(0).getValue() < 10) {
            doubles = getGreaterThanOrEqual(doubles);
            if (doubles.size() > 0) {
                update(doubles, DOUBLE);
                return true;
            } else if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 2 && table.getState().get(0).getValue() >= 10) {
            doubles = getGreaterThanOrEqual(doubles);
            if (doubles.size() > 0 && doubles.get(0).getValue() == table.getState().get(0).getValue() && (table.getState().get(0).getSuit() == 4 || table.getState().get(1).getSuit() == 4)) {
                doubles.remove(0);
                doubles.remove(0);
            }
            if (doubles.size() > 0) {
                update(doubles, DOUBLE);
                return true;
            } else if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 3) {
            if (isValidMatch(table.getState())) {
                triples = getGreaterThanOrEqual(triples);
                if (triples.size() > 0) {
                    update(triples, TRIPLE);
                    return true;
                }
            }
            if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 4) {
            bombs = getGreaterThanOrEqual(bombs);
            if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 5) {
            if (bombs.size() > 0) {
                update(bombs, BOMB);
                return true;
            }
            return false;
        } else if (table.getState().size() == 6) {
            return false;
        }
        return false;
    }

    private boolean isValidMatch(List<Card> cards) {
        HashSet<Integer> set = new HashSet<>();
        for(Card card: cards)
            set.add(card.getValue());
        return set.size() == 1;
    }

    private List<Card> getGreaterThanOrEqual(List<Card> cards) {
        List<Card> temp = new ArrayList<>();
        for (Card card: cards) {
            if (card.getValue() >= table.getState().get(0).getValue()) {
                temp.add(card);
            }
        }
        return temp;
    }

    private List<Card> getSingles() {
        List<Card> temp = new ArrayList<>();
        for(List<Card> check: preSingles) {
            for(Card card: check) {
                temp.add(card);
            }
        }
        return temp;
    }

    private List<Card> getDoubles() {
        List<Card> temp = new ArrayList<>();
        for(List<Card> check: preDoubles) {
            for(Card card: check) {
                temp.add(card);
            }
        }
        return temp;
    }

    private List<Card> getTriples() {
        List<Card> temp = new ArrayList<>();
        for(List<Card> check: preTriples) {
            for(Card card: check) {
                temp.add(card);
            }
        }
        return temp;
    }

    private List<Card> getBombs() {
        List<Card> temp = new ArrayList<>();
        for(List<Card> check: preBomb) {
            for(Card card: check) {
                temp.add(card);
            }
        }
        return temp;
    }

    private void clear() {
        three.clear();
        four.clear();
        five.clear();
        six.clear();
        seven.clear();
        eight.clear();
        nine.clear();
        ten.clear();
        eleven.clear();
        twelve.clear();
        thirteen.clear();
        fourteen.clear();
        fifteen.clear();

        preSingles.clear();
        preDoubles.clear();
        preTriples.clear();
        preBomb.clear();
    }

    private void update(List<Card> cards, int number) {
        table.getState().clear();
        for(int i = 0; i < number; i++) {
            table.getState().add(i, cards.get(i));
            hand.remove(cards.get(i));
        }
        cards.clear();
    }

    private void organizeCards() {
        for(Card card: hand) {
            if (card.getValue() == 3) {
                three.add(card);
            } else if (card.getValue() == 4) {
                four.add(card);
            } else if (card.getValue() == 5) {
                five.add(card);
            } else if (card.getValue() == 6) {
                six.add(card);
            } else if (card.getValue() == 7) {
                seven.add(card);
            } else if (card.getValue() == 8) {
                eight.add(card);
            } else if (card.getValue() == 9) {
                nine.add(card);
            } else if (card.getValue() == 10) {
                ten.add(card);
            } else if (card.getValue() == 11) {
                eleven.add(card);
            } else if (card.getValue() == 12) {
                twelve.add(card);
            } else if (card.getValue() == 13) {
                thirteen.add(card);
            } else if (card.getValue() == 14) {
                fourteen.add(card);
            } else if (card.getValue() == 15) {
                fifteen.add(card);
            }
        }
        List<List<Card>> temp = new ArrayList<>();
        temp.add(three);
        temp.add(four);
        temp.add(five);
        temp.add(six);
        temp.add(seven);
        temp.add(eight);
        temp.add(nine);
        temp.add(ten);
        temp.add(eleven);
        temp.add(twelve);
        temp.add(thirteen);
        temp.add(fourteen);
        temp.add(fifteen);

        for(List<Card> check: temp) {
            if (check.size() == 1) {
                preSingles.add(check);
            } else if (check.size() == 2) {
                preDoubles.add(check);
            } else if (check.size() == 3) {
                preTriples.add(check);
            } else if (check.size() == 4) {
                preBomb.add(check);
            }
        }
    }
}
