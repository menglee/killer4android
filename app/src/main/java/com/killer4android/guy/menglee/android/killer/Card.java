package com.killer4android.guy.menglee.android.killer;

import android.support.annotation.NonNull;

/**
 * Created by menglee on 2/24/2016.
 */
public class Card implements Comparable<Card> {

    private int mValue;
    private int mSuit;

    public Card(int value, int suit) {
        this.mValue = value;
        this.mSuit = suit;
    }

    public int getValue() {
        return mValue;
    }

    public int getSuit() {
        return mSuit;
    }

    @Override
    public int compareTo(@NonNull Card card) {
        if (this.mValue == card.mValue)
            return 0;
        else
            return this.mValue > card.mValue ? 1 : -1;
    }

    public String toString() {
        switch (mSuit) {
            case 1: { return "Club"; }
            case 2: { return "Diamond"; }
            case 3: { return "Heart"; }
            case 4: { return "Spade"; }
        }
        return "error toString()";
    }

    public int toInt() {
        String temp = Integer.toString(mValue) + Integer.toString(mSuit);
        return Integer.parseInt(temp);
    }
}
