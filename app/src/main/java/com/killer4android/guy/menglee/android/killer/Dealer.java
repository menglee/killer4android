package com.killer4android.guy.menglee.android.killer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by menglee on 2/24/2016.
 */
public class Dealer {
    //private static final String TAG = "GameActivity";
    private List<Card> deck = new ArrayList<>();

    private final int MAX_CARDS = 52;

    private List<Card> mHandOne = new ArrayList<>();
    private List<Card> mHandTwo = new ArrayList<>();
    private List<Card> mHandThree = new ArrayList<>();
    private List<Card> mHandFour = new ArrayList<>();

    public Dealer(List<Card> deck) {
        this.deck = deck;
    }

    public List<Card> getHandOne() {
        return mHandOne;
    }

    public List<Card> getHandTwo() {
        return mHandTwo;
    }

    public List<Card> getHandThree() {
        return mHandThree;
    }

    public List<Card> getHandFour() {
        return mHandFour;
    }

    public void shuffle() {
        Collections.shuffle(deck);
        Collections.shuffle(deck);
    }

    public void deal() {
        for (int i = 0; i < MAX_CARDS; i += 4)
        {
            mHandOne.add(deck.get(i));
            mHandTwo.add(deck.get(i + 1));
            mHandThree.add(deck.get(i + 2));
            mHandFour.add(deck.get(i + 3));
        }
        Collections.sort(mHandOne);
        Collections.sort(mHandTwo);
        Collections.sort(mHandThree);
        Collections.sort(mHandFour);
    }
}
